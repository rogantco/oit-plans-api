const mongoose = require('mongoose');
const {Schema} = require('mongoose');
mongoose.Promise = require('bluebird');
const crypto = require('crypto');

const UserSchema = new Schema({
    provider: String,
    name: String,
    email: {
        type: String,
        lowercase: true,
        required: true
    },
    role: {
        type: String,
        default: 'user'
    },
    password: String,
    salt: {
        type: String,
    }
}, {timestamps: true});

// Public profile information
UserSchema
    .virtual('profile')
    .get(function() {
        return {
            '_id': this._id,
            'name': this.name,
            'role': this.role,
            'email': this.email,
        };
    });

// Non-sensitive info we'll be putting in the token
UserSchema
    .virtual('token')
    .get(function() {
        return {
            '_id': this._id,
            'role': this.role
        };
    });

// Validate empty password
UserSchema
    .path('password')
    .validate(function(password) {
        return password.length;
    }, 'Password cannot be blank');

// Validate email is not taken
UserSchema
    .path('email')
    .validate(function(value) {
        return this.constructor.findOne({ email: value }).exec()
            .then(user => {
                if(user) {
                    if(this.id === user.id) {
                        return true;
                    }
                    return false;
                }
                return true;
            })
            .catch(function(err) {
                throw err;
            });
    }, 'The specified email address is already in use.');

let validatePresenceOf = function(value) {
    return value && value.length;
};

/**
 * Pre-save hook
 */
UserSchema
    .pre('save', function(next) {
        // Handle new/update passwords
        if(!this.isModified('password')) {
            return next();
        }

        if (!validatePresenceOf(this.password)) {
            next(new Error('Invalid password'));
        }

        // Make salt with a callback
        this.makeSalt((saltErr, salt) => {
            if(saltErr) {
                return next(saltErr);
            }
            this.salt = salt;
            this.encryptPassword(this.password, (encryptErr, hashedPassword) => {
                if(encryptErr) {
                    return next(encryptErr);
                }
                this.password = hashedPassword;
                return next();
            });
        });
    });

/**
 * Methods
 */
UserSchema.methods = {
    /**
     * Authenticate - check if the passwords are the same
     *
     * @param {String} password
     * @param {Function} callback
     * @return {Boolean}
     * @api public
     */
    authenticate(password, callback) {
        if(!callback) {
            return this.password === this.encryptPassword(password);
        }

        this.encryptPassword(password, (err, pwdGen) => {
            if(err) {
                return callback(err);
            }

            if(this.password === pwdGen) {
                return callback(null, true);
            } else {
                return callback(null, false);
            }
        });
    },

    /**
     * Make salt
     *
     * @param {Number} [byteSize] - Optional salt byte size, default to 16
     * @param {Function} callback
     * @return {String}
     * @api public
     */
    makeSalt(...args) {
        let byteSize;
        let callback;
        let defaultByteSize = 16;

        if(typeof args[0] === 'function') {
            callback = args[0];
            byteSize = defaultByteSize;
        } else if(typeof args[1] === 'function') {
            callback = args[1];
        } else {
            throw new Error('Missing Callback');
        }

        if(!byteSize) {
            byteSize = defaultByteSize;
        }

        return crypto.randomBytes(byteSize, (err, salt) => {
            if(err) {
                return callback(err);
            } else {
                return callback(null, salt.toString('base64'));
            }
        });
    },

    /**
     * Encrypt password
     *
     * @param {String} password
     * @param {Function} callback
     * @return {String}
     * @api public
     */
    encryptPassword(password, callback) {
        if(!password || !this.salt) {
            if(!callback) {
                return null;
            } else {
                return callback('Missing password or salt');
            }
        }

        let defaultIterations = 10000;
        let defaultKeyLength = 64;
        let salt = new Buffer(this.salt, 'base64');

        if(!callback) {
            // eslint-disable-next-line no-sync
            return crypto.pbkdf2Sync(password, salt, defaultIterations,
                defaultKeyLength, 'sha1')
                .toString('base64');
        }

        return crypto.pbkdf2(password, salt, defaultIterations, defaultKeyLength, 'sha1', (err, key) => {
            if(err) {
                return callback(err);
            } else {
                return callback(null, key.toString('base64'));
            }
        });
    }
};

module.exports =  mongoose.model('User', UserSchema);
