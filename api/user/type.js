const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLID,
    GraphQLInputObjectType
} = require('graphql');
const {GraphQLDateTime} = require('graphql-iso-date');

const UserType = new GraphQLObjectType({
    name: 'User',
    fields: () => ({
        _id: {
            type: GraphQLID,
        },
        name: {
            type: GraphQLString,
        },
        email: {
            type: GraphQLString,
        },
        role: {
            type: GraphQLString,
        },
        createdAt: {
            type: GraphQLDateTime
        },
        updatedAt: {
            type: GraphQLDateTime
        }
    })
});

const UserMutationType = new GraphQLInputObjectType ({
    name: 'User_Input',
    fields: {
        name: {
            type: GraphQLString
        },
        email: {
            type: GraphQLString
        },
        role: {
            type: GraphQLString
        },
        password: {
            type: GraphQLString
        }
    }
});

module.exports = {
    UserType,
    UserMutationType
};
