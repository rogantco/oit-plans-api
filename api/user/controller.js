const _ = require('lodash');
const User = require('./model');

/**
 * Get list of users
 * restriction: 'admin'
 */
function index(root, argss, res) {
    let query = argss._id ? {_id: argss._id} : {};
    return User.find(query, '-salt -password');
}

/**
 * Creates a new user
 */
function create(root, argss) {
    argss.input.provider = 'local';
    return User.create(argss.input);
}

function update(root, argss) {
    return User.findById(argss._id).then((entity) => {
        const updated = _.merge(entity, argss.input);
        return updated.save();
    });
}

// Deletes a Currency from the DB
function destroy(_id) {
    return User.findByIdAndRemove(_id);
}

module.exports = {
    index,
    create,
    update,
    destroy
};
