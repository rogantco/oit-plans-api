const controller = require('./controller');
const {
    GraphQLString,
    GraphQLList,
    GraphQLID,
    GraphQLNonNull
} = require('graphql');
const {UserType, UserMutationType} = require('./type');

const userQuery = {
    type: new GraphQLList(UserType),
    args: {
        _id: { type: GraphQLString }
    },
    resolve: (root, args, res) =>  {
        return controller.index(root, args, res)
    }
};

const userMutation = {
    type: UserType,
    args: {
        _id: {
            type: GraphQLID
        },
        input: {
            type: UserMutationType
        }
    },
    resolve: (root, args, res) =>  {
        if(args._id) {
            return controller.update(root, args, res);
        } else {
            return controller.create(root, args, res);
        }
    }
};

const userDelete = {
    type: new GraphQLList(UserType),
    args: {
        _ids: {
            type: new GraphQLList(new GraphQLNonNull(GraphQLID))
        }
    },
    resolve: (root, args, res) =>  {
        const deletePromises = args._ids.map((_id) => {
            return controller.destroy(_id);
        });

        return Promise.all(deletePromises);
    }
};

module.exports = {
    userQuery,
    userMutation,
    userDelete
};
