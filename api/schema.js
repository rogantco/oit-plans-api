const {
    GraphQLSchema,
    GraphQLObjectType
} = require('graphql');
const { userQuery, userMutation, userDelete } = require('./user/schema');
const session = require('./session/schema');
const { planQuery, planMutation, planDelete } = require('./plan/schema');
const { currencyQuery, currencyMutation, currencyDelete } = require('./currency/schema');
const { countryQuery, countryMutation, countryDelete } = require('./country/schema');
const { cityQuery, cityMutation, cityDelete } = require('./city/schema');
const { offerQuery } = require('./offer/schema');

module.exports = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Main_Query',
        fields: {
            user: userQuery,
            session,
            plan: planQuery,
            currency: currencyQuery,
            country: countryQuery,
            city: cityQuery,
            offer: offerQuery
        },
    }),
    mutation: new GraphQLObjectType({
        name: 'Main_Mutation',
        fields: {
            user: userMutation,
            plan: planMutation,
            country: countryMutation,
            city: cityMutation,
            currency: currencyMutation,
            currencyDelete,
            userDelete,
            planDelete,
            countryDelete,
            cityDelete
        }
    })
});
