const _ = require('lodash');
const Country = require('./model');

/**
 * Get list of Plans
 */
function index(root, argss, res) {
    let query = argss._id ? {_id: argss._id} : {};
    return Country.find(query);
}

// Creates a new Plan in the DB
function create(root, argss) {
    return Country.create(argss.input)
}

// Updates an existing Restaurant in the DB
function update(root, argss) {
    return Country.findById(argss._id).then((entity) => {
        const updated = _.merge(entity, argss.input);
        return updated.save();
    });
}

// Deletes a Currency from the DB
function destroy(_id) {
    return Country.findByIdAndRemove(_id);
}

module.exports = {
    index,
    create,
    update,
    destroy
};
