const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLID,
    GraphQLInputObjectType
} = require('graphql');
const {GraphQLDateTime} = require('graphql-iso-date');
const CountryType = new GraphQLObjectType({
    name: 'Country',
    fields: () => ({
        _id: {
            type: GraphQLID,
        },
        name: {
            type: GraphQLString,
        },
        isoCode: {
            type: GraphQLString,
        },
        createdAt: {
            type: GraphQLDateTime
        },
        updatedAt: {
            type: GraphQLDateTime
        }
    })
});

const CountryMutationType = new GraphQLInputObjectType ({
    name: 'Country_Input',
    fields: {
        name: {
            type: GraphQLString
        },
        isoCode: {
            type: GraphQLString
        },
    }
});

module.exports = {
    CountryType,
    CountryMutationType
};
