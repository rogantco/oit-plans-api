const mongoose = require('mongoose');
const {Schema} = require('mongoose');
mongoose.Promise = require('bluebird');

const CountrySchema = new Schema({
    name: String,
    isoCode: {
        type: String,
        uppercase: true
    }
}, {timestamps: true});

module.exports = mongoose.model('Country', CountrySchema);
