const controller = require('./controller');
const {
    GraphQLString,
    GraphQLList,
    GraphQLID,
    GraphQLNonNull
} = require('graphql');
const { CountryType, CountryMutationType } = require('./type');

const countryQuery = module.exports = {
    type: new GraphQLList(CountryType),
    args: {
        _id: { type: GraphQLString }
    },
    resolve: (root, args, res) =>  {
        return controller.index(root, args, res)
    }
};

const countryMutation = {
    type: CountryType,
    args: {
        _id: {
            type: GraphQLID
        },
        input: {
            type: CountryMutationType
        }
    },
    resolve: (root, args, res) =>  {
        if(args._id) {
            return controller.update(root, args, res);
        } else {
            return controller.create(root, args, res);
        }
    }
};

const countryDelete = {
    type: new GraphQLList(CountryType),
    args: {
        _ids: {
            type: new GraphQLList(new GraphQLNonNull(GraphQLID))
        }
    },
    resolve: (root, args, res) =>  {
        const deletePromises = args._ids.map((_id) => {
            return controller.destroy(_id);
        });

        return Promise.all(deletePromises);
    }
};

module.exports = {
    countryQuery,
    countryMutation,
    countryDelete
};
