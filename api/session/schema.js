const User = require('../user/model');
const passport = require('passport');
const {signToken} = require('./service');
const {
    GraphQLString,
} = require('graphql');
const sessionType = require('./type');

module.exports = {
    type: sessionType,
    args: {
        email: { type: GraphQLString },
        password: { type: GraphQLString }
    },
    resolve: (root, args, req, res) =>  {
        req.body = {email: args.email, password: args.password};

        return new Promise((resolve, reject) => {
            require('./passport').setup(User);

            passport.authenticate('local', function(err, user, info) {
                let error = err || info;
                if (error) {
                    reject(error);
                }
                if (!user) {
                    reject({message: 'Something went wrong, please try again.'});
                }

                let sessionInfo = {
                    _id: user._id,
                    name: user.name,
                    email: user.email,
                    token: signToken(user._id, user.role)
                };
                resolve(sessionInfo)
            })(req, res);
        });
    }
};
