const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLID
} = require('graphql');

module.exports = new GraphQLObjectType({
    name: 'session',
    description: '...',

    fields: () => ({
        _id: {
            type: GraphQLID
        },
        email: {
            type: GraphQLString
        },
        name: {
            type: GraphQLString
        },
        token: {
            type: GraphQLString,
        }
    })
});
