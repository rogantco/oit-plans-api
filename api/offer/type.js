const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLID,
    GraphQLList
} = require('graphql');
const {GraphQLDateTime} = require('graphql-iso-date');

const OfferType = new GraphQLObjectType({
    name: 'Offer',
    fields: () => ({
        _id: {
            type: GraphQLID,
        },
        name: {
            type: GraphQLString,
        },
        email: {
            type: GraphQLString,
        },
        city: {
            type: GraphQLString,
        },
        phone: {
            type: GraphQLString,
        },
        plans: {
            type:  new GraphQLList(new GraphQLObjectType({
                name: 'Offer_plan',
                fields: {
                    _id: {
                        type: GraphQLID,
                    },
                    name: {
                        type: GraphQLString,
                    }
                }
            }))
        },
        createdAt: {
            type: GraphQLDateTime
        },
        updatedAt: {
            type: GraphQLDateTime
        }
    })
});

module.exports = {
    OfferType
};
