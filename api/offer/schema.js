const controller = require('./controller');
const {
    GraphQLList,
    GraphQLID,
} = require('graphql');
const {OfferType} = require('./type');

const offerQuery = {
    type: new GraphQLList(OfferType),
    args: {
        _id: { type: GraphQLID }
    },
    resolve: (root, args, res) =>  {
        return controller.index(root, args, res)
    }
};

module.exports = {
    offerQuery
};
