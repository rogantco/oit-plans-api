const mongoose = require('mongoose');
const {Schema} = require('mongoose');
mongoose.Promise = require('bluebird');

const OfferSchema = new Schema({
    name: String,
    email: String,
    city: String,
    phone: String,
    plans: [{
        type: Schema.Types.ObjectId,
        ref: 'Plan'
    }],
}, {timestamps: true});

module.exports = mongoose.model('Offer', OfferSchema);
