const _ = require('lodash');
const Offer = require('./model');

function handleError(res, statusCode) {
    statusCode = statusCode || 500;
    return function(err) {
        res.status(statusCode).send(err);
    };
}

function responseWithResult(res, statusCode) {
    statusCode = statusCode || 200;
    return function(entity) {
        if (entity) {
            res.status(statusCode).json(entity);
        }
    };
}

/**
 * Get list of Currencies
 */
function index(root, argss, res) {
    let query = argss._id ? {_id: argss._id} : {};
    return Offer.find(query).populate([
        'plans'
    ]);
}

// Creates a new Plan in the DB
function create(req, res) {
    Offer.create(req.body)
        .then(responseWithResult(res, 201))
        .catch(handleError(res));
}

module.exports = {
    create,
    index
};
