const controller = require('./controller');
const {
    GraphQLString,
    GraphQLList,
    GraphQLID,
    GraphQLNonNull
} = require('graphql');
const {CurrencyType, CurrencyMutationType} = require('./type');

const currencyQuery = {
    type: new GraphQLList(CurrencyType),
    args: {
        _id: { type: GraphQLString }
    },
    resolve: (root, args, res) =>  {
        return controller.index(root, args, res)
    }
};

const currencyMutation = {
    type: CurrencyType,
    args: {
        _id: {
            type: GraphQLID
        },
        input: {
            type: CurrencyMutationType
        }
    },
    resolve: (root, args, res) =>  {
        if(args._id) {
            return controller.update(root, args, res);
        } else {
            return controller.create(root, args, res);
        }
    }
};

const currencyDelete = {
    type: new GraphQLList(CurrencyType),
    args: {
        _ids: {
            type: new GraphQLList(new GraphQLNonNull(GraphQLID))
        }
    },
    resolve: (root, args, res) =>  {
        const deletePromises = args._ids.map((_id) => {
            return controller.destroy(_id);
        });

        return Promise.all(deletePromises);
    }
};

module.exports = {
    currencyQuery,
    currencyMutation,
    currencyDelete
};
