const _ = require('lodash');
const Currency = require('./model');

/**
 * Get list of Currencies
 */
function index(root, argss, res) {
    let query = argss._id ? {_id: argss._id} : {};
    return Currency.find(query);
}

// Creates a new Currency in the DB
function create(root, argss) {
    return Currency.create(argss.input)
}

// Updates an existing Currency in the DB
function update(root, argss) {
    return Currency.findById(argss._id).then((entity) => {
        const updated = _.merge(entity, argss.input);
        return updated.save();
    });
}

// Deletes a Currency from the DB
function destroy(_id) {
    return Currency.findByIdAndRemove(_id);
}

module.exports = {
    index,
    create,
    update,
    destroy
};
