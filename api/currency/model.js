const mongoose = require('mongoose');
const {Schema} = require('mongoose');
mongoose.Promise = require('bluebird');

const CurrencySchema = new Schema({
    name: String,
    code: {
        type: String,
        uppercase: true
    },
    symbol: String
}, {timestamps: true});

module.exports = mongoose.model('Currency', CurrencySchema);
