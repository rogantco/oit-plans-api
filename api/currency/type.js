const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLID,
    GraphQLInputObjectType
} = require('graphql');
const {GraphQLDateTime} = require('graphql-iso-date');

const CurrencyType = new GraphQLObjectType({
    name: 'Currency',
    fields: () => ({
        _id: {
            type: GraphQLID,
        },
        name: {
            type: GraphQLString,
        },
        code: {
            type: GraphQLString,
        },
        symbol: {
            type: GraphQLString,
        },
        createdAt: {
            type: GraphQLDateTime
        },
        updatedAt: {
            type: GraphQLDateTime
        }
    })
});

const CurrencyMutationType = new GraphQLInputObjectType ({
    name: 'Currency_Input',
    fields: {
        name: {
            type: GraphQLString
        },
        code: {
            type: GraphQLString
        },
        symbol: {
            type: GraphQLString
        },
    }
});

module.exports = {
    CurrencyType,
    CurrencyMutationType
};
