const _ = require('lodash');
const Plan = require('./model');

/**
 * Get list of Plans
 */
function index(root, argss) {
    let query = argss._id ? {_id: argss._id} : {};
    return Plan.find(query).populate([
        'cities',
        'prices.currency',
        'countries'
    ]);
}

// Creates a new Plan in the DB
function create(root, argss) {
    return Plan.create(argss.input)
}

// Updates an existing Restaurant in the DB
function update(root, argss) {
    return Plan.findById(argss._id).then((entity) => {
        const updated = _.merge(entity, argss.input);
        return updated.save();
    });
}

// Deletes a Currency from the DB
function destroy(_id) {
    return Plan.findByIdAndRemove(_id);
}

module.exports = {
    index,
    create,
    update,
    destroy
};
