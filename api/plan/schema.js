const controller = require('./controller');
const {
    GraphQLString,
    GraphQLList,
    GraphQLID,
    GraphQLNonNull
} = require('graphql');
const { PlanType, PlanMutationType } = require('./type');

const planQuery = {
    type: new GraphQLList(PlanType),
    args: {
        _id: { type: GraphQLString }
    },
    resolve: (root, args, res) =>  {
        return controller.index(root, args, res)
    }
};

const planMutation = {
    type: PlanType,
    args: {
        _id: {
            type: GraphQLID
        },
        input: {
            type: PlanMutationType
        }
    },
    resolve: (root, args, res) =>  {
        if(args._id) {
            return controller.update(root, args, res);
        } else {
            return controller.create(root, args, res);
        }
    }
};

const planDelete = {
    type: new GraphQLList(PlanType),
    args: {
        _ids: {
            type: new GraphQLList(new GraphQLNonNull(GraphQLID))
        }
    },
    resolve: (root, args, res) =>  {
        const deletePromises = args._ids.map((_id) => {
            return controller.destroy(_id);
        });

        return Promise.all(deletePromises);
    }
};

module.exports = {
    planQuery,
    planMutation,
    planDelete
};
