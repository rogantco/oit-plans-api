const mongoose = require('mongoose');
const {Schema} = require('mongoose');
mongoose.Promise = require('bluebird');

const PlanSchema = new Schema({
    name: String,
    days: Number,
    countries: [{
        type: Schema.Types.ObjectId,
        ref: 'Country'
    }],
    cities: [{
        type: Schema.Types.ObjectId,
        ref: 'City'
    }],
    prices: [{
        label: String,
        amount: Number,
        currency: {
            type: Schema.Types.ObjectId,
            ref: 'Currency'
        },
        applies: [{
            from: Date,
            to: Date
        }]
    }],
    departures: {
        label: String,
        dates: [{
            date: Date
        }]
    },
    itinerary: [{
        title: String,
        description: String
    }],
    include: [{
        text: String
    }],
    notInclude : [{
        text: String
    }]
}, {timestamps: true});

module.exports = mongoose.model('Plan', PlanSchema);
