const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLInt,
    GraphQLList,
    GraphQLID,
    GraphQLInputObjectType
} = require('graphql');
const {GraphQLDate, GraphQLDateTime} = require('graphql-iso-date');
const {CountryType} = require('./../country/type');
const {CityType} = require('./../city/type');
const {CurrencyType} = require('./../currency/type');

const PlanType = new GraphQLObjectType({
    name: 'Plan',
    fields: {
        _id: {
            type: GraphQLID
        },
        name: {
            type: GraphQLString
        },
        days: {
            type: GraphQLInt
        },
        countries: {
            type:  new GraphQLList(CountryType)
        },
        cities: {
            type:  new GraphQLList(CityType)
        },
        prices: {
            type: new GraphQLList(new GraphQLObjectType({
                name: 'Plan_prices',
                fields: {
                    _id: {
                        type: GraphQLID,
                    },
                    label: {
                        type: GraphQLString,
                    },
                    amount: {
                        type: GraphQLInt,
                    },
                    currency: {
                        type:  CurrencyType
                    },
                    applies :{
                        type: new GraphQLList(new GraphQLObjectType({
                            name: 'Plan_prices_applies',
                            fields: {
                                _id: {
                                    type: GraphQLID,
                                },
                                from: {
                                    type: GraphQLDate
                                },
                                to: {
                                    type: GraphQLDate
                                }
                            }
                        }))
                    }
                }
            })),
        },
        departures: {
            type: new GraphQLObjectType({
                name: 'Plan_departures',
                fields: {
                    label: {
                        type: GraphQLString
                    },
                    dates: {
                        type: new GraphQLList(new GraphQLObjectType({
                            name: 'Plan_departures_dates',
                            fields: {
                                _id: {
                                    type: GraphQLID,
                                },
                                date: {
                                    type: GraphQLDate
                                }
                            }
                        }))
                    }
                }
            })
        },
        itinerary: {
            type: new GraphQLList(new GraphQLObjectType({
                name: 'Plan_itinerary',
                fields: {
                    title: {
                        type: GraphQLString
                    },
                    description: {
                        type: GraphQLString
                    }
                }
            }))
        },
        include: {
            type: new GraphQLList(new GraphQLObjectType({
                name: 'Plan_include',
                fields: {
                    text: {
                        type: GraphQLString
                    }
                }
            }))
        },
        notInclude : {
            type: new GraphQLList(new GraphQLObjectType({
                name: 'Plan_notInclude',
                fields: {
                    text: {
                        type: GraphQLString
                    }
                }
            }))
        },
        createdAt: {
            type: GraphQLDateTime
        },
        updatedAt: {
            type: GraphQLDateTime
        }
    }
});

const PlanMutationType = new GraphQLInputObjectType ({
    name: 'Plan_Input',
    fields: {
        name: {
            type: GraphQLString
        },
        days: {
            type: GraphQLInt
        },
        countries: {
            type:  new GraphQLList(new GraphQLInputObjectType({
                name: 'Plan_Input_countries',
                fields: {
                    _id: {
                        type: GraphQLID,
                    }
                }
            }))
        },
        cities: {
            type:  new GraphQLList(new GraphQLInputObjectType({
                name: 'Plan_Input_cities',
                fields: {
                    _id: {
                        type: GraphQLID,
                    }
                }
            }))
        },
        prices: {
            type: new GraphQLList(new GraphQLInputObjectType({
                name: 'Plan_Input_prices',
                fields: {
                    _id: {
                        type: GraphQLID,
                    },
                    label: {
                        type: GraphQLString,
                    },
                    amount: {
                        type: GraphQLInt,
                    },
                    currency: {
                        type:  new GraphQLInputObjectType({
                            name: 'Plan_Input_currency',
                            fields: {
                                _id: {
                                    type: GraphQLID,
                                }
                            }
                        })
                    },
                    applies :{
                        type: new GraphQLList(new GraphQLInputObjectType({
                            name: 'Plan_Input_prices_applies',
                            fields: {
                                _id: {
                                    type: GraphQLID,
                                },
                                from: {
                                    type: GraphQLDate
                                },
                                to: {
                                    type: GraphQLDate
                                }
                            }
                        }))
                    }
                }
            })),
        },
        departures: {
            type: new GraphQLInputObjectType({
                name: 'Plan_Input_departures',
                fields: {
                    label: {
                        type: GraphQLString
                    },
                    dates: {
                        type: new GraphQLList(new GraphQLInputObjectType({
                            name: 'Plan_Input__dates',
                            fields: {
                                _id: {
                                    type: GraphQLID,
                                },
                                date: {
                                    type: GraphQLDate
                                }
                            }
                        }))
                    }
                }
            })
        },
        itinerary: {
            type: new GraphQLList(new GraphQLInputObjectType({
                name: 'Plan_Input_itinerary',
                fields: {
                    title: {
                        type: GraphQLString
                    },
                    description: {
                        type: GraphQLString
                    }
                }
            }))
        },
        include: {
            type: new GraphQLList(new GraphQLInputObjectType({
                name: 'Plan_Input_include',
                fields: {
                    text: {
                        type: GraphQLString
                    }
                }
            }))
        },
        notInclude : {
            type: new GraphQLList(new GraphQLInputObjectType({
                name: 'Plan_Input_notInclude',
                fields: {
                    text: {
                        type: GraphQLString
                    }
                }
            }))
        }
    }
});

module.exports = {
    PlanType,
    PlanMutationType
};
