const controller = require('./controller');
const {
    GraphQLString,
    GraphQLList,
    GraphQLID,
    GraphQLNonNull
} = require('graphql');
const {CityType, CityMutationType} = require('./type');

const cityQuery = {
    type: new GraphQLList(CityType),
    args: {
        _id: { type: GraphQLString }
    },
    resolve: (root, args, res) =>  {
        return controller.index(root, args, res)
    }
};

const cityMutation = {
    type: CityType,
    args: {
        _id: {
            type: GraphQLID
        },
        input: {
            type: CityMutationType
        }
    },
    resolve: (root, args, res) =>  {
        if(args._id) {
            return controller.update(root, args, res);
        } else {
            return controller.create(root, args, res);
        }
    }
};

const cityDelete = {
    type: new GraphQLList(CityType),
    args: {
        _ids: {
            type: new GraphQLList(new GraphQLNonNull(GraphQLID))
        }
    },
    resolve: (root, args, res) =>  {
        const deletePromises = args._ids.map((_id) => {
            return controller.destroy(_id);
        });

        return Promise.all(deletePromises);
    }
};

module.exports = {
    cityQuery,
    cityMutation,
    cityDelete
};
