const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLID,
    GraphQLInputObjectType
} = require('graphql');
const {GraphQLDateTime} = require('graphql-iso-date');
const {CountryType} = require('./../country/type');

const CityType = new GraphQLObjectType({
    name: 'City',
    fields: () => ({
        _id: {
            type: GraphQLID,
        },
        name: {
            type: GraphQLString,
        },
        country: {
            type: CountryType,
        },
        createdAt: {
            type: GraphQLDateTime
        },
        updatedAt: {
            type: GraphQLDateTime
        }
    })
});

const CityMutationType = new GraphQLInputObjectType ({
    name: 'City_Input',
    fields: {
        name: {
            type: GraphQLString
        },
        country: {
            type: new GraphQLInputObjectType({
                name: 'City_Input_Country',
                fields: {
                    _id: {
                        type: GraphQLID,
                    }
                }
            })
        }
    }
});

module.exports = {
    CityType,
    CityMutationType
};
