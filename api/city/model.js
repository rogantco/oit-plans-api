const mongoose = require('mongoose');
const {Schema} = require('mongoose');
mongoose.Promise = require('bluebird');

const CitySchema = new Schema({
    name: String,
    country: {
        type: Schema.Types.ObjectId,
        ref: 'Country'
    },
    coordinates: {
        type: [Number],
        index: '2dsphere'
    }
}, {timestamps: true});

module.exports = mongoose.model('City', CitySchema);
