const _ = require('lodash');
const City = require('./model');

/**
 * Get list of Plans
 */
function index(root, argss, res) {
    let query = argss._id ? {_id: argss._id} : {};
    return City.find(query).populate([
        'country'
    ]);
}

// Creates a new Plan in the DB
function create(root, argss) {
    return City.create(argss.input)
}

// Updates an existing Restaurant in the DB
function update(root, argss) {
    return City.findById(argss._id).then((entity) => {
        const updated = _.merge(entity, argss.input);
        return updated.save();
    });
}

// Deletes a Currency from the DB
function destroy(_id) {
    return City.findByIdAndRemove(_id);
}

module.exports = {
    index,
    create,
    update,
    destroy
};
