module.exports = function(app) {
    // Insert routes below
    app.use('/gql', require('./api'));
    app.use('/offer', require('./api/offer'));

    // All other routes should redirect a 404
    app.route('/*').get((req, res) => {
        res.status(404).send("Sorry can't find that!")
    });
};
