module.exports = {
    // Secret for session, you will want to change this and make it an environment variable
    secrets: {
        session: 'oitBackPrivateSecret'
    },

    // MongoDB connection options
    mongo: {
        options: {
            db: {
                safe: true
            }
        }
    },
    userRoles: ['guest', 'user', 'admin']
};
