const express = require('express');
const mongoose = require('mongoose');
const connectMongo = require('connect-mongo');
const session = require('express-session');
const  cors  = require('cors');
const morgan = require('morgan');

const mongoStore = connectMongo(session);
const passport = require('passport');
const config = require('./config');
const bodyParser = require('body-parser');

mongoose.connect('mongodb://127.0.0.1/oit_plans', {
    useMongoClient: true
});
mongoose.connection.on('error', function(err) {
    console.error('MongoDB connection error: ' + err);
    process.exit(-1);
});
//require('./seed');

const app = express();
app.use(morgan('[:date[clf]]: :remote-addr - :remote-user ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent"'));
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

//FIXES CORS ERROR
const whitelist = [
    'http://localhost:3000',
    'http://losmejoresviajes.co:5000',
    'http://www.losmejoresviajes.co:5000'
];
const corsOptions = {
    origin: function(origin, callback){
        let originIsWhitelisted = whitelist.indexOf(origin) !== -1;
        callback(null, originIsWhitelisted);
    },
    credentials: true
};
app.use(cors(corsOptions));

app.use(passport.initialize());
app.use(session({
    secret: config.secrets.session,
    saveUninitialized: true,
    resave: false,
    store: new mongoStore({
        mongooseConnection: mongoose.connection,
        db: 'oit_plans'
    })
}));

require('./routes')(app);

app.listen(4000, () => console.log('Example app listening on port 4000'));
