const User = require('./api/user/model');
const Currency = require('./api/currency/model');
const Country = require('./api/country/model');
const City = require('./api/city/model');
const Plan = require('./api/plan/model');

//Populate Users
User.find({}).remove().then(() => {
    populateUsers();
});

function populateUsers(){
    return User.create({
        provider: 'local',
        name: 'Pepito Vargas',
        email: 'rogant+domi@gmail.com',
        password: 'test'
    }, {
        provider: 'local',
        role: 'admin',
        name: 'Andres Vargas',
        email: 'rogant@gmail.com',
        password: 'admin'
    });
}



const deletePromises = [
    Currency.find({}).remove(),
    Country.find({}).remove(),
    City.find({}).remove(),
    Plan.find({}).remove()
];

Promise.all(deletePromises).then(function() {
    populateCities();
    populateCurrencies();
    populatePlanOne();
});

function populatePlanOne() {
    Country.create({
        name: 'España',
        isoCode: 'Es'
    }).then((country) => {
        City.create({
            name: 'Madrid',
            country: country._id
        }).then((city) => {
            Currency.create({
                name: 'Euro',
                code: 'EUR',
                symbol: '€'
            }).then((currency) => {
                Plan.create({
                    name: 'Europa Test 1',
                    days: '14',
                    countries: [country._id],
                    cities: [city._id],
                    prices: [{
                        label: 'Temporada Alta',
                        amount: 999,
                        currency: currency._id,
                        applies: [{
                            from:  new Date(1989, 6, 15),
                            to:  new Date(1989, 8, 30)
                        },
                            {
                                from:  new Date(1989, 10, 15),
                                to:  new Date(1989, 12, 31)
                            }]
                        },
                        {
                        label: 'Temporada Baja',
                        amount: 799,
                        currency: currency._id,
                        applies: [{
                            from:  new Date(1989, 1, 1),
                            to:  new Date(1989, 6, 14)
                        },
                        {
                            from:  new Date(1989, 9, 1),
                            to:  new Date(1989, 10, 14)
                        }]
                    }],
                    departures: {
                        label: 'Lunes de 2017 - 2018',
                        dates: [{
                            date: new Date(2017, 10, 20),
                        },{
                            date: new Date(2017, 10, 27),
                        },{
                            date: new Date(2017, 11, 4),
                        }]
                    },
                    itinerary: [{
                        title: 'DÍA 01. COLOMBIA - PARÍS',
                        description: 'Salida en vuelo internacional con destino a la ciudad de\n' +
                        'París. Noche a bordo.'
                    },{
                        title: 'DÍA 02. PARÍS',
                        description: 'Llegada a Paris, asistencia y traslado al hotel. Resto del día\n' +
                        'libre. Alojamiento.'
                    },{
                        title: 'DÍA 02. PARÍS',
                        description: 'Desayuno, a la hora convenida visita panorámica de la\n' +
                        'ciudad de la Luz, recorriendo la Avenida de los Campos\n' +
                        'Elíseos, Arco del Triunfo, Torre Eiffel, Isla de la Ciudad, breve\n' +
                        'tiempo para visitar la Medalla Milagrosa, entre otros. Tarde\n' +
                        'libre para actividades personales. Alojamiento.'
                    },{
                        title: 'DÍA 02. PARÍS',
                        description: 'Desayuno. Día libre a disposición para efectuar excursiones\n' +
                        'opcionales y continuar descubriendo esta fascinante ciu-\n' +
                        'dad. Alojamiento.'
                    }],
                    include: [{
                        text: 'Alojamiento y desayunos diarios.'
                    },{
                        text: 'Vuelo interno Roma – Madrid.'
                    },{
                        text: 'Maleta de mano 10 kilos trayecto Roma – Madrid.'
                    },{
                        text: 'Guía acompañante durante todo recorrido.'
                    }],
                    notInclude : [{
                        text: 'Tiquetes aéreos (internacionales e internos no\n' +
                        'mencionados).'
                    },{
                        text: 'Valor aproximado equipaje de 20 kilos en trayecto\n' +
                        'Roma – Madrid 20 USD.'
                    },{
                        text: 'Tarjeta integral de asistencia al viajero Assist Card.'
                    },{
                        text: 'Impuesto fee bancario 2 % sobre el valor de la porción\n' +
                        'terrestre.'
                    }]
                })
            });
        });
    });
}


function populateCurrencies(){
    return Currency.create({
        name: 'Dollar',
        code: 'USD',
        symbol: '$'
    }, {
        name: 'Peso Colombiano',
        code: 'COP',
        symbol: '$'
    });
}

function populateCities(){
    Country.create({
        name: 'Francia',
        isoCode: 'Fr'
    }).then((result) => {
        City.create({
            name: 'Paris',
            country: result._id
        });
    });

    Country.create({
        name: 'Italia',
        isoCode: 'It'
    }).then((result) => {
        City.create({
            name: 'Roma',
            country: result._id
        });
    });
}
